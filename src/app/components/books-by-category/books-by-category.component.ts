import { Component, OnInit } from '@angular/core';
import {IBook} from '../../interfaces/IBook';
import {BooksService} from '../../services/books.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-books-by-category',
  templateUrl: './books-by-category.component.html',
  styleUrls: ['./books-by-category.component.css']
})
export class BooksByCategoryComponent implements OnInit {

  books: IBook[] = [];

  constructor(
    private bs: BooksService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.books = [];
    const categoryId = this.route.snapshot.params.categoryId;

    this.bs.getBooksByCategoryId(categoryId).subscribe(data => {
      this.books = data;
    });
  }

}
