import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { IBook } from 'src/app/interfaces/IBook';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  
  books: IBook[] = [];
  
  constructor(private bs: BooksService) { }

  ngOnInit() {
    this.books = [];

    this.bs.getBooks().subscribe(data => {
      this.books = data;
    });
  }

}
