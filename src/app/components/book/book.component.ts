import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { IBook } from 'src/app/interfaces/IBook';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  book: IBook = {};
  constructor(
    private bs: BooksService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.spinner = true;
    const bookId: string = this.router.snapshot.params.id;
    this.bs.getBook(bookId).subscribe(data => {
      this.book = data;
      // this.spinner = false;
    });
  }

}
