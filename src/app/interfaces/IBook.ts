export interface IBook {
    id?: string;
    title?: string;
    author?: string;
    photo?: string;
    description?: string;
    category?: string;
}
