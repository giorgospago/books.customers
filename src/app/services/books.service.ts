import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBook } from '../interfaces/IBook';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient) { }

  getBooks(key?: string) {
    return this.http.get<IBook[]>('http://localhost:3000/customer/books/' + (key || ''));
  }

  getBooksByCategoryId(categoryId: string) {
    return this.http.get<IBook[]>('http://localhost:3000/customer/books-by-category/' + categoryId);
  }

  getBook(id: string) {
    return this.http.get<IBook>('http://localhost:3000/customer/book/' + id);
  }
}
