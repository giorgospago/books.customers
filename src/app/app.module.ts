import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BooksComponent } from './components/books/books.component';
import { BookComponent } from './components/book/book.component';
import { HttpClientModule } from '@angular/common/http';
import { BooksByCategoryComponent } from './components/books-by-category/books-by-category.component';

const appRoutes: Routes = [
  {path: '', component: BooksComponent},
  {path: 'category/:categoryId', component: BooksByCategoryComponent},
  {path: 'book/:id', component: BookComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookComponent,
    BooksByCategoryComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
